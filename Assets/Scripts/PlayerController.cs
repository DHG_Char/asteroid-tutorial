﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Boundary
{
    public float xMin, xMax, zMin, zMax;
}

public class PlayerController : MonoBehaviour {

    private Rigidbody rb;
    public float speed;
    public float tilt;
    public Boundary bounds;
    public GameObject shot;
    public Transform shotSpawn;
    private float nextFire;
    public float shotDelay;
    private AudioSource sound;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
        sound = GetComponent<AudioSource>();
        nextFire = 0.0f;
    }

    // Called just before each fixed physics step.
    void FixedUpdate()
    {
        float moveHorizontal = Input.GetAxis("Horizontal");
        float moveVertical = Input.GetAxis("Vertical");

        Vector3 delta = new Vector3(moveHorizontal, 0.0f, moveVertical);
        rb.velocity = delta * speed;
        rb.position = new Vector3
        (
            Mathf.Clamp(rb.position.x,bounds.xMin, bounds.xMax),
            0.0f,
            Mathf.Clamp(rb.position.z, bounds.zMin, bounds.zMax)
        );

        rb.rotation = Quaternion.Euler(0.0f, 0.0f, rb.velocity.x * -tilt);
    }

    void Update()
    {
        if (Input.GetButton("Fire1") && Time.time > nextFire)
        {
            nextFire = Time.time + shotDelay;
            Instantiate(shot, shotSpawn.position, shotSpawn.rotation);
            sound.Play();
        }
    }
}
