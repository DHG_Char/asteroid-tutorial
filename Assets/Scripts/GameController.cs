﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameController : MonoBehaviour {

    public GameObject hazard;
    public Vector3 spawnValues;
    private float timeDelay;
    public float minTime, maxTime;
    public int numPerWave;
    private int numSpawned;
    public float waveWait;
    public Text scoreText;
    public Text restartText;
    public Text gameOverText;
    private bool gameOver;
    private bool restart;
    private int score;

    void SpawnWaves()
    {
        Vector3 spawnPosition = new Vector3(Random.Range(-spawnValues.x, spawnValues.x), spawnValues.y, spawnValues.z);
        Instantiate(hazard, spawnPosition, Quaternion.identity);
        numSpawned++;
    }

	// Use this for initialization
	void Start () {
        timeDelay = 5;
        score = 0;
        gameOver = false;
        restart = false;
        restartText.text = "";
        gameOverText.text = "";
        UpdateScore();
    }
	
	// Update is called once per frame
	void Update () {
        if (!gameOver)
        {
            if (Time.time > timeDelay)
            {
                timeDelay = Time.time + Random.Range(minTime, maxTime);
                if (numSpawned > numPerWave)
                {
                    timeDelay += waveWait;
                    numSpawned = 0;
                }
                else
                {
                    SpawnWaves();
                }
            }
        }
        else if(!restart)
        {
            restartText.text = "Press 'R' to restart";
            restart = true;
        }
        else
        {
            if (restart && Input.GetKeyDown(KeyCode.R))
            {
                SceneManager.LoadScene("asteroid");
            }
        }
    }

    void UpdateScore()
    {
        scoreText.text = "Score : " + score.ToString();
    }

    public void AddScore(int newScoreValue)
    {
        score += newScoreValue;
        UpdateScore();
    }

    public void GameOver()
    {
        gameOver = true;
        gameOverText.text = "Game Over";
    }
}
