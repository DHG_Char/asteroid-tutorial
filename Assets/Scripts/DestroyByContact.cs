﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyByContact : MonoBehaviour {
    public GameObject explosion;
    public GameObject playerExplosion;
    public int scoreValue;
    private GameController controller;
    
    void Start()
    {
        GameObject controlObj = GameObject.FindGameObjectWithTag("GameController");
        if(controlObj != null)
        {
            controller = controlObj.GetComponent<GameController>();
        }        
        if(controller == null)
        {
            Debug.Log("Cannot find GameController script.");
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if (!other.CompareTag("Boundary"))
        {
            Instantiate(explosion, transform.position, transform.rotation);
            if (other.CompareTag("Player"))
            {
                Instantiate(playerExplosion, other.transform.position, other.transform.rotation);
                controller.GameOver();
            }
            else
            {
                controller.AddScore(scoreValue);
            }
            Destroy(other.gameObject);
            Destroy(gameObject);
        }
    }
}
